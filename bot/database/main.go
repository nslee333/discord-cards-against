package database

import (
	"context"
	"fmt"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Database struct {
	Mongo *mongo.Client
}

func NewDatabase() (*Database, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	// Username, password, host is all in the environment

	mongouri := fmt.Sprintf(
		"mongodb://%s:%s@%s:%s/?authSource=admin",
		os.Getenv("MONGO_USERNAME"),
		os.Getenv("MONGO_PASSWORD"),
		os.Getenv("MONGO_HOST"),
		os.Getenv("MONGO_PORT"),
	)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongouri))

	if err != nil {
		return &Database{}, err
	}

	return &Database{
		Mongo: client,
	}, nil
}
