package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/mchineboy/cahbot/bot/database"
	"github.com/mchineboy/cahbot/bot/discord"

	"net/http"
	_ "net/http/pprof"

	"github.com/bwmarrin/discordgo"
)

func main() {
	go func() {
		log.Println("Starting pprof server on port 6060")
		log.Println(http.ListenAndServe(":6060", nil))
	}()

	if err := runBot(); err != nil {
		log.Fatal(err)
	}
}

func runBot() error {
	token := os.Getenv("DISCORD_TOKEN")
	if token == "" {
		return fmt.Errorf("missing token environment variable")
	}

	// client := &http.Client{
	// 	Timeout: 10 * time.Second,
	// }

	b, err := discordgo.New("Bot " + token)
	if err != nil {
		return err
	}

	b.StateEnabled = true

	mongoDb, err := database.NewDatabase()
	if err != nil {
		log.Println(err.Error())
	}

	bot := discord.NewBot(b, mongoDb.Mongo)

	if err != nil {
		return fmt.Errorf("could not get my user: %w", err)
	}

	b.Identify.Intents = discordgo.IntentsAll

	b.AddHandler(bot.CommandsHandler)
	b.AddHandler(bot.InteractionHandler)
	b.AddHandler(bot.JoinGuild)

	err = b.Open()
	if err != nil {
		return fmt.Errorf("could not connect to discord: %w", err)
	}

	go func() {
		defer handlePanic(b)
	}()

	log.Print("Discord bot is now running. Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, syscall.SIGTERM)
	<-sc

	return b.Close()
}

func handlePanic(b *discordgo.Session) {
	if r := recover(); r != nil {
		log.Printf("Panic: %s", r)
	}
}

