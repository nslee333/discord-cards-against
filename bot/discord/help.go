package discord

import (
	"fmt"
	"log"

	"github.com/bwmarrin/discordgo"
)

func (b *Bot) Help(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	// Using b.Commands, generate a help message
	// Send the help message to the channel

	helpString := ""

	for _, c := range b.Commands {
		helpString += fmt.Sprintf("%s%s - %s\n", b.ActivationSigil, c.Name, c.Description)
	}
	embed := &discordgo.MessageEmbed{
		Title: "CardsBot Help",
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:  "Commands",
				Value: helpString,
			},
			{
				Name:  "Like the Bot?",
				Value: "If you like the bot, consider [buying me a coffee](https://www.buymeacoffee.com/yellerjeep) or [supporting me on Patreon](https://www.patreon.com/yellerjeep_tr).",
			},
			{
				Name:  "Want this bot in your server?",
				Value: "You can add this bot to your server by [clicking here](https://discord.com/api/oauth2/authorize?client_id=1174383107563061299&permissions=18685256133696&scope=bot).",
			},
			{
				Name:  "Want to help develop this bot?",
				Value: "You can find the source code for this bot on [GitLab](https://gitlab.com/tylerhardison/discord-cards-against).",
			},
			{
				Name:  "Have an issue or bug report?",
				Value: "You can report issues or bugs on [GitLab](https://gitlab.com/tylerhardison/discord-cards-against/-/issues).",
			},
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: s.State.User.AvatarURL("128"),
		},
	}
	if m != nil {

		messageSend := discordgo.MessageSend{
			Embed: embed,
		}

		// Send announcement message
		_, err := b.ResponseToEventComplex(
			s,
			m,
			i,
			&messageSend,
			true,
		)

		if err != nil {
			log.Printf("Help: Error sending message: %s", err)
			return err
		}
	} else if i != nil {
		// Respond to InteractionCreate event
		response := discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Embeds: []*discordgo.MessageEmbed{
					embed,
				},
				Flags: discordgo.MessageFlagsEphemeral,
			},
		}

		err := s.InteractionRespond(i.Interaction, &response)

		if err != nil {
			log.Printf("Error responding to interaction: %s", err.Error())
			return err
		}
	}
	return nil
}
