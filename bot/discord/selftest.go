package discord

import (
	"context"
	"log"

	"github.com/bwmarrin/discordgo"
	"go.mongodb.org/mongo-driver/bson"
)

func (b *Bot) SelfTest(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	// Test the round trip to the mongodb, write a record, read a record, delete a record, do not crash.
	// Select the discordbot database
	discordbot := b.MongoClient.Database("discord-cah-bot")

	// Select the test collection

	testCollection := discordbot.Collection("test")

	// Insert a record

	_, err := testCollection.InsertOne(context.Background(), bson.M{"test": "test"})
	if err != nil {
		_, err := b.RespondToEvent(s, m, i, "mongo test: cannot insert", true)
		if err != nil {
			log.Printf("Selftest: Error sending message: %s", err.Error())
		}
		return err
	}

	// Read the record

	var result bson.M

	err = testCollection.FindOne(context.Background(), bson.M{}).Decode(&result)
	if err != nil {
		_, err := b.RespondToEvent(s, m, i, "mongo test: cannot read", true)
		if err != nil {
			log.Printf("Selftest: Error sending message: %s", err.Error())
		}
		return err
	}

	// Delete the record

	_, err = testCollection.DeleteOne(context.Background(), bson.M{})
	if err != nil {
		_, err := b.RespondToEvent(s, m, i, "mongo test: cannot delete", true)
		if err != nil {
			log.Printf("Selftest: Error sending message: %s", err.Error())
		}
		return err
	}

	_, err = b.RespondToEvent(s, m, i, "mongo test: success", true)

	if err != nil {
		log.Printf("Selftest: Error sending message: %s", err.Error())
	}
	return nil
}
