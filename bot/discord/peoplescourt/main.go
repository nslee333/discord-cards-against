package peoplescourt

import "go.mongodb.org/mongo-driver/mongo"

type PeoplesCourt struct {
	Database *mongo.Database
}

func NewPeoplesCourt(database *mongo.Database) *PeoplesCourt {
	return &PeoplesCourt{
		Database: database,
	}
}

// Players can take each other to court.
// They can sue each other for money, items, or other things.

