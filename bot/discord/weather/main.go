package weather

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"time"

	owm "github.com/briandowns/openweathermap"
)

type GeonamesResponse struct {
	Geonames []struct {
		CountryName string `json:"countryName"`
		ToponymName string `json:"toponymName"`
		Lat         string `json:"lat"`
		Lng         string `json:"lng"`
	} `json:"geonames"`
}

func GetWeatherByLocation(location string, locale string) (owm.OneCallData, GeonamesResponse, int, error) {
	geonamesUsername := os.Getenv("GEONAMES_USERNAME")
	// Location is probably city, state, and possibly country. We need to use geonames.org to get a lat/long
	// for the location, then use that lat/long to get the weather.
	// Parse the location string into city, state, and country
	// Valid inputs are:
	// Bare City Name
	// City, State
	// City, State, Country
	// City, Country
	// State, Country
	// Country
	// We also allow for +[0-9] to get a forecast for X days in the future

	// Does the location contain a +? If so, we need to parse that out and get the forecast

	// \+([0-9]+)
	// \+([0-9]+)d
	// \+([0-9]+)days
	// \+([0-9]+)day
	// \+([0-9]+)d
	var err error
	forecastDays := 0

	parseDays := regexp.MustCompile(`\+([0-9]+).*$`)

	if parseDays.MatchString(location) {
		forecastDays, err = strconv.Atoi(parseDays.FindStringSubmatch(location)[1])
		if err != nil {
			return owm.OneCallData{}, GeonamesResponse{}, forecastDays, err
		}
	}

	// Remove the days from the location string

	location = parseDays.ReplaceAllString(location, "")

	url := "http://api.geonames.org/searchJSON?q=" + url.QueryEscape(location) + "&maxRows=1&username=" + geonamesUsername

	response, err := http.Get(url)
	if err != nil {
		return owm.OneCallData{}, GeonamesResponse{}, forecastDays, err
	}

	defer response.Body.Close()

	// Decode the response
	var geonamesResponse GeonamesResponse
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return owm.OneCallData{}, geonamesResponse, forecastDays, err
	}

	err = json.Unmarshal(body, &geonamesResponse)
	if err != nil {
		return owm.OneCallData{}, geonamesResponse, forecastDays, err
	}

	latitude, err := strconv.ParseFloat(geonamesResponse.Geonames[0].Lat, 64)
	if err != nil {
		return owm.OneCallData{}, geonamesResponse, forecastDays, err
	}
	longitude, err := strconv.ParseFloat(geonamesResponse.Geonames[0].Lng, 64)
	if err != nil {
		return owm.OneCallData{}, geonamesResponse, forecastDays, err
	}
	geoLocation := &owm.Coordinates{
		Latitude:  latitude,
		Longitude: longitude,
	}

	w, err := owm.NewOneCall("F", "EN", os.Getenv("OPENWEATHERMAP_API_KEY"), []string{})
	if err != nil {
		return *w, geonamesResponse, forecastDays, err
	}

	err = w.OneCallByCoordinates(geoLocation)
	if err != nil {
		return *w, geonamesResponse, forecastDays, err
	}

	return *w, geonamesResponse, forecastDays, nil
}

func GetForecastMarkdown(w owm.OneCallData, geodata GeonamesResponse, forecast int) string {
	forecastString := ""

	if forecast > 0 {
		// Get the forecast
		forecastString = forecastString + fmt.Sprintf("# Weather Forecast for %s, %s", geodata.Geonames[0].ToponymName, geodata.Geonames[0].CountryName) + "\n"
		for _, v := range w.Daily {
			date := time.Unix(int64(v.Dt), 0)
			forecastString = forecastString + "## " + date.Format("2006-01-02") + "\n"
			forecastString = forecastString + "Conditions: "
			for _, w := range v.Weather {
				forecastString = forecastString + w.Main + " " + w.Description + " "
			}
			forecastString = forecastString + "\n"
			forecastString = forecastString + "Temp: " + strconv.FormatFloat(v.Temp.Day, 'f', 2, 64) +
				"°f (" + strconv.FormatFloat(ConvertCelsius(v.Temp.Day), 'f', 2, 64) + "°c)\n"
			forecastString = forecastString + "High: " + strconv.FormatFloat(v.Temp.Max, 'f', 2, 64) +
				"°f (" + strconv.FormatFloat(ConvertCelsius(v.Temp.Max), 'f', 2, 64) + "°c)\n"
			forecastString = forecastString + "Low: " + strconv.FormatFloat(v.Temp.Min, 'f', 2, 64) +
				"°f (" + strconv.FormatFloat(ConvertCelsius(v.Temp.Min), 'f', 2, 64) + "°c)\n\n"

		}
	} else {
		forecastString = forecastString + fmt.Sprintf("# Current weather for %s, %s", geodata.Geonames[0].ToponymName, geodata.Geonames[0].CountryName) + "\n"
		forecastString = forecastString + "Conditions: "
		forecastString = forecastString + w.Current.Weather[0].Main + " " + w.Current.Weather[0].Description + " "
		forecastString = forecastString + "\n"
		forecastString = forecastString + "Temp: " + strconv.FormatFloat(w.Current.Temp, 'f', 2, 64) +
			"°f (" + strconv.FormatFloat(ConvertCelsius(w.Current.Temp), 'f', 2, 64) + "°c)\n"
		forecastString = forecastString + "Feels Like: " + strconv.FormatFloat(w.Current.FeelsLike, 'f', 2, 64) +
			"°f (" + strconv.FormatFloat(ConvertCelsius(w.Current.FeelsLike), 'f', 2, 64) + "°c)\n"
		forecastString = forecastString + "High: " + strconv.FormatFloat(w.Daily[0].Temp.Max, 'f', 2, 64) +
			"°f (" + strconv.FormatFloat(ConvertCelsius(w.Daily[0].Temp.Max), 'f', 2, 64) + "°c)\n"
		forecastString = forecastString + "Low: " + strconv.FormatFloat(w.Daily[0].Temp.Min, 'f', 2, 64) +
			"°f (" + strconv.FormatFloat(ConvertCelsius(w.Daily[0].Temp.Min), 'f', 2, 64) + "°c)\n\n"
		forecastString = forecastString + "UVI: " + strconv.FormatFloat(w.Current.UVI, 'f', 2, 64) + "\n"
		forecastString = forecastString + "Wind Speed: " + strconv.FormatFloat(w.Current.WindSpeed, 'f', 2, 64) +
			"mph (" + strconv.FormatFloat(ConvertMphToKph(w.Current.WindSpeed), 'f', 2, 64) + "kph)\n"
	}
	return forecastString
}

func ConvertCelsius(f float64) float64 {
	return (f - 32) * 5 / 9
}

func ConvertMphToKph(f float64) float64 {
	return f * 1.609344
}
