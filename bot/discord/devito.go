package discord

import (
	"log"
	"math/rand"
	"os"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func (b *Bot) Devito(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	file, err := os.ReadFile("../data/danny.txt")
	channelId := b.GetChannelId(m, i)
	if err != nil {
		_, err := s.ChannelMessageSend(channelId, "Error reading file: "+err.Error())
		if err != nil {
			log.Printf("Danny: Error sending message: %s", err.Error())
		}
		return err
	}
	lines := strings.Split(string(file), "\n")

	_, err = s.ChannelMessageSend(channelId, lines[rand.Intn(len(lines))]+" - Danny Devito")
	if err != nil {
		log.Printf("Danny: Error sending message: %s", err.Error())
		return err
	}

	return err
}
