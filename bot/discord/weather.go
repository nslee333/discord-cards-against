package discord

import (
	"fmt"
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/mchineboy/cahbot/bot/discord/weather"
)

func (b *Bot) Weather(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {

	// Get user's timezone

	user, err := b.GetUser(s, m, i)

	if err != nil {
		log.Printf("Weather: Error getting user: %s", err.Error())
	}

	w, geodata, forecast, err := weather.GetWeatherByLocation(strings.Join(arguments, " "), user.Locale)
	if err != nil {
		_, err := b.RespondToEvent(s, m, i, fmt.Sprintf("error getting weather for %s", strings.Join(arguments, " ")), true)
		if err != nil {
			log.Printf("Weather Error: Error sending message: %s", err.Error())
		}
		return err
	}

	_, err = b.RespondToEvent(s, m, i, weather.GetForecastMarkdown(w, geodata, forecast), false)

	if err != nil {
		log.Printf("Weather: Error sending message: %s", err.Error())
		return err
	}

	return nil
}
