package horoscope

// Using the openai api to generate a horoscope for the user.

import (
	"context"
	"fmt"
	"os"

	openai "github.com/sashabaranov/go-openai"
)

func GetUsersHoroscope(username string, sign string) string {
	// Strip new lines from api key
	key := os.Getenv("OPENAI_API_KEY")
	if len(key) > 0 && key[len(key)-1] == '\n' {
		key = key[:len(key)-1]
	}
	client := openai.NewClient(key)

	req := openai.ChatCompletionRequest{
		Model: openai.GPT4TurboPreview,
		Messages: []openai.ChatCompletionMessage{
			{
				Role:    openai.ChatMessageRoleSystem,
				Content: fmt.Sprintf("Generate a funny horoscope in two paragraphs for %s who believes they are a %s, be creative and its ok to be PG-13. Finally, generate a random number that is funny in context of the horoscope.", username, sign),
			},
		},
	}

	resp, err := client.CreateChatCompletion(context.Background(), req)

	if err != nil {
		fmt.Printf("ChatCompletion error: %v\n", err)
		return ""
	}

	return resp.Choices[0].Message.Content
}
