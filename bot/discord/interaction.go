package discord

import (
	"fmt"
	"log"

	"github.com/bwmarrin/discordgo"
)

func (b *Bot) InteractionHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	// Handle the interaction with buttons and stuff

	switch i.Type {
	case discordgo.InteractionApplicationCommand:
		data := i.ApplicationCommandData()

		// Get the arguments that were passed
		var commandArgs []string = []string{}
		var commandName string

		if len(data.Options) > 0 && data.Options[0].Type == discordgo.ApplicationCommandOptionSubCommand {

			// Subcommand
			commandName = data.Name
			commandArgs = append(commandArgs, data.Options[0].Name)
			for _, option := range data.Options[0].Options {

				switch option.Type {
				case discordgo.ApplicationCommandOptionString:
					commandArgs = append(commandArgs, option.StringValue())
				case discordgo.ApplicationCommandOptionInteger:
					commandArgs = append(commandArgs, fmt.Sprintf("%d", option.IntValue()))
				case discordgo.ApplicationCommandOptionBoolean:
					commandArgs = append(commandArgs, fmt.Sprintf("%t", option.BoolValue()))
				case discordgo.ApplicationCommandOptionUser:
					commandArgs = append(commandArgs, option.UserValue(s).ID)
				case discordgo.ApplicationCommandOptionChannel:
					commandArgs = append(commandArgs, option.ChannelValue(s).ID)
				}
			}
		} else {
			commandName = data.Name
			for _, option := range data.Options {
				switch option.Type {
				case discordgo.ApplicationCommandOptionString:
					commandArgs = append(commandArgs, option.StringValue())
				case discordgo.ApplicationCommandOptionInteger:
					commandArgs = append(commandArgs, fmt.Sprintf("%d", option.IntValue()))
				case discordgo.ApplicationCommandOptionBoolean:
					commandArgs = append(commandArgs, fmt.Sprintf("%t", option.BoolValue()))
				case discordgo.ApplicationCommandOptionUser:
					commandArgs = append(commandArgs, option.UserValue(s).ID)
				case discordgo.ApplicationCommandOptionChannel:
					commandArgs = append(commandArgs, option.ChannelValue(s).ID)
				}
			}
		}
		log.Printf("Got command %s with args %v", commandName, commandArgs)

		// Scan b.Commands for the command
		for _, c := range b.Commands {
			if c.Name == commandName {
				// We found the command, run it
				err := c.Function(s, nil, i, commandArgs)
				if err != nil {
					log.Printf("Error running command %s: %s", c.Name, err.Error())
				}
				return
			}
		}
	case discordgo.InteractionMessageComponent:
		// Get the game from the map
		if _, ok := games[i.ChannelID]; !ok {
			// Game doesn't exist
			return
		}
		game := games[i.ChannelID]
		// Call the game's interaction handler
		game.InteractionHandler(s, i)
		return
	}
}
