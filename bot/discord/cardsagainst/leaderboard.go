package cardsagainst

import (
	"context"
	"fmt"
	"image"
	"image/color"
	"image/gif"
	"log"
	"net/http"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"github.com/fogleman/gg"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type LeaderBoard struct {
	Users []PlayerScore `json:"users"`
}

type PlayerScore struct {
	DiscordID   string `bson:"DiscordID,omitempty"`
	Name        string `bson:"Name,omitempty"`
	TotalPoints int    `bson:"TotalPoints,omitempty"`
	TotalWins   int    `bson:"TotalWins,omitempty"`
}

func (c *CardsAgainst) GetScoreboard() ([]PlayerScore, error) {
	var scoreboard []PlayerScore
	cursor, err := c.Mongo.Collection("scoreboard").Find(context.Background(), bson.M{})
	if err != nil {
		return nil, err
	}
	err = cursor.All(context.Background(), &scoreboard)
	return scoreboard, err
}

func (c *CardsAgainst) SaveScoreboard(scoreboard []PlayerScore) error {
	_, err := c.Mongo.Collection("scoreboard").DeleteMany(context.Background(), bson.M{})
	if err != nil {
		return err
	}

	var scoreboardInterface []interface{}
	for _, playerScore := range scoreboard {
		scoreboardInterface = append(scoreboardInterface, playerScore)
	}

	_, err = c.Mongo.Collection("scoreboard").InsertMany(context.Background(), scoreboardInterface)
	return err
}

func (c *CardsAgainst) GetRankedScores() ([]PlayerScore, error) {
	var scoreboard []PlayerScore
	opts := options.Find().SetSort(bson.D{primitive.E{Key: "TotalPoints", Value: -1}})
	cursor, err := c.Mongo.Collection("scoreboard").Find(context.Background(), bson.M{}, opts)
	if err != nil {
		return nil, err
	}
	err = cursor.All(context.Background(), &scoreboard)
	return scoreboard, err
}

func (c *CardsAgainst) GetRankedWins() ([]PlayerScore, error) {
	var scoreboard []PlayerScore
	opts := options.Find().SetSort(bson.D{primitive.E{Key: "TotalWins", Value: -1}})
	cursor, err := c.Mongo.Collection("scoreboard").Find(context.Background(), bson.M{}, opts)
	if err != nil {
		return nil, err
	}
	err = cursor.All(context.Background(), &scoreboard)
	return scoreboard, err
}

func (c *CardsAgainst) UpdatePlayerScores(player Player, Wins int, Score int) error {
	// Pull the user's current stored score
	result := c.Mongo.Collection("scoreboard").FindOne(context.Background(), bson.M{"DiscordID": player.DiscordID})

	var playerScore PlayerScore

	err := result.Decode(&playerScore)
	if err != nil {
		log.Printf("Error decoding player score: %s", err.Error())
	}

	// Was the user found?
	if playerScore.DiscordID == "" {
		// User was not found, create a new record
		playerScore = PlayerScore{
			DiscordID:   player.DiscordID,
			Name:        player.Name,
			TotalPoints: Score,
			TotalWins:   Wins,
		}
		_, err = c.Mongo.Collection("scoreboard").InsertOne(context.Background(), playerScore)
		return err
	}
	// Update the player's score
	playerScore.TotalPoints += Score
	playerScore.TotalWins += Wins

	_, err = c.Mongo.Collection("scoreboard").UpdateOne(
		context.Background(),
		bson.M{"DiscordID": player.DiscordID},
		bson.D{
			primitive.E{Key: "$set", Value: bson.D{primitive.E{Key: "TotalPoints", Value: playerScore.TotalPoints}}},
			primitive.E{Key: "$set", Value: bson.D{primitive.E{Key: "TotalWins", Value: playerScore.TotalWins}}},
			primitive.E{Key: "$setOnInsert", Value: bson.D{primitive.E{Key: "Name", Value: player.Name}}},
		},
		options.Update().SetUpsert(true),
	)
	return err
}

func (c *CardsAgainst) GenerateScoreboard(s *discordgo.Session) error {
	const W = 800
	const H = 800
	const S = 75      // Spacing
	const Margin = 50 // Added margin constant

	// Create a new image context
	dc := gg.NewContext(W, H)

	// Draw a gradient background from top left to bottom right
	// Starting with light green and ending with dark green
	grad := gg.NewLinearGradient(0, 0, W, H)
	grad.AddColorStop(0, color.RGBA{114, 144, 137, 255})
	grad.AddColorStop(1, color.RGBA{52, 101, 89, 255})

	dc.SetRGB(1, 1, 1)

	dc.DrawRectangle(0, 0, W, H)
	dc.SetFillStyle(grad)
	dc.Fill()

	// Get the guild image to go into the middle, make it a circle

	// Draw the Guild Icon at the lower left of the card
	resp, err := http.Get(c.Guild.IconURL("64"))
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	img, _, err := image.Decode(resp.Body)
	if err != nil {
		img, err = gif.Decode(resp.Body)
		if err != nil {
			log.Printf("Error decoding image: %v %s", err, c.Guild.IconURL("64"))
		}
	}

	// Only draw the image if it was successfully decoded
	if img != nil {
		dc.DrawImage(img, Margin, H-Margin-img.Bounds().Dy())
	}

	// Draw the Guild Name as the lower left text
	err = dc.LoadFontFace("../fonts/DejaVuSans.ttf", 24)

	if err != nil {
		log.Println(err)
	}

	dc.SetRGB(1, 1, 1)
	// String should be left aligned
	dc.DrawStringAnchored(fmt.Sprintf("Cards Against %s", c.Guild.Name), float64(Margin+img.Bounds().Dx()+8), float64(H-Margin-32), 0, 1)

	err = dc.LoadFontFace("../fonts/DejaVuSans-Bold.ttf", 48)
	if err != nil {
		return err
	}

	// Fetch sorted scores
	winners, err := c.GetRankedWins()

	if err != nil {
		return err
	}

	// Draw Winners Column
	dc.DrawStringAnchored("Winners", W*0.25, S, 0.5, 0.5)

	err = dc.LoadFontFace("../fonts/DejaVuSans-Bold.ttf", 28)
	if err != nil {
		return err
	}
	for i, winner := range winners {

		member, err := s.GuildMember(c.Guild.ID, winner.DiscordID)
		if err != nil {
			log.Printf("(Generate Scoreboard) Error getting member: %s", err.Error())
		}

		nickname := winner.Name
		if member != nil && member.Nick != "" {
			nickname = member.Nick
		}

		// Restrict nickname to 12 characters
		if len(nickname) > 12 {
			nickname = nickname[:12]
		}

		user, err := s.User(winner.DiscordID)
		if err != nil {
			log.Printf("Error getting user: %s", err.Error())
		}
		resp, err := http.Get(user.AvatarURL("64"))
		if err != nil {
			log.Println(err)
		}
		defer resp.Body.Close()

		img, _, err := image.Decode(resp.Body)
		if err != nil {
			img, err = gif.Decode(resp.Body)
			if err != nil {
				log.Printf("Error decoding image: %v %s", err, user.AvatarURL("64"))
			}
		}
		// Calculate Y position accounting for image height + 8px padding
		y := 80 * (float64(i) + 2.5)

		// Only draw the image if it was successfully decoded
		if img != nil {
			dc.DrawImage(img, int(W*.0625), int(y-32))
		}

		// Draw the nickname to the right of the avatar image
		dc.DrawStringAnchored(nickname+": "+strconv.Itoa(winner.TotalWins), W*0.0625+float64(img.Bounds().Dx()+8), y, 0.0, 0.5)

		if i == 4 {
			break
		}
	}

	// Fetch sorted points
	scorers, err := c.GetRankedScores()
	if err != nil {
		return err
	}

	err = dc.LoadFontFace("../fonts/DejaVuSans-Bold.ttf", 48)
	if err != nil {
		return err
	}

	// Draw Scorers Column
	dc.DrawStringAnchored("Top Scorers", W*0.725, S, 0.5, 0.5)

	err = dc.LoadFontFace("../fonts/DejaVuSans-Bold.ttf", 28)
	if err != nil {
		return err
	}
	for i, scorer := range scorers {
		y := 80 * (float64(i) + 2.5) // Calculate Y position

		member, err := s.GuildMember(c.Guild.ID, scorer.DiscordID)
		if err != nil {
			log.Printf("(Generating Scoreboard) Error getting member: %s", err.Error())
		}

		nickname := scorer.Name
		if member != nil && member.Nick != "" {
			nickname = member.Nick
		}

		// Restrict nickname to 12 characters
		if len(nickname) > 12 {
			nickname = nickname[:12]
		}

		user, err := s.User(scorer.DiscordID)
		if err != nil {
			log.Printf("Error getting user: %s", err.Error())
		}
		resp, err := http.Get(user.AvatarURL("64"))
		if err != nil {
			log.Println(err)
		}
		defer resp.Body.Close()

		img, _, err := image.Decode(resp.Body)
		if err != nil {
			img, err = gif.Decode(resp.Body)
			if err != nil {
				log.Printf("Error decoding image: %v %s", err, user.AvatarURL("64"))
			}
		}

		// Only draw the image if it was successfully decoded
		if img != nil {
			dc.DrawImage(img, int(W*0.50), int(y-32))
		}

		dc.DrawStringAnchored(nickname+": "+strconv.Itoa(scorer.TotalPoints), W*0.50+float64(img.Bounds().Dx()+8), y, 0.0, 0.5)

		if i == 4 {
			break
		}
	}

	err = dc.SavePNG(fmt.Sprintf("scoreboard-%s.png", c.Guild.ID))

	if err != nil {
		return err
	}

	return nil
}
