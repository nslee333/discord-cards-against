package cardsagainst

import "github.com/bwmarrin/discordgo"

func (c *CardsAgainst) ResetCommand(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate) {
	c.GameState.CurrentState = GameOver

	// This is so the game timer will be exited.

	_, err := c.SendResponse(s, m, i, []string{"Game has been reset!"}, true)
	if err != nil {
		return
	}
}
