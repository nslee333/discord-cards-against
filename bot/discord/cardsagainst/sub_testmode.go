package cardsagainst

import (
	"log"
	"time"

	"github.com/bwmarrin/discordgo"
)

// This function creates three random users and sets up a game to run through.
// It's used for testing the game logic.

func (c *CardsAgainst) TestGame(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, guild *discordgo.Guild, activationSigil string) {
	channelId := c.GetChannelId(m, i)

	log.Printf("Starting test game in channel %s", channelId)

	_, err := s.ChannelMessageSend(channelId, "Starting test game")

	if err != nil {
		log.Printf("Error sending message: %s", err.Error())
		return
	}

	err = c.StartGameCommand(s, m, i, guild, activationSigil)
	if err != nil {
		log.Printf("Error starting game: %s", err.Error())
		return
	}

	for index := 0; index < 5; index++ {
		_, err = c.JoinGameCommand(s, m, i)
		if err != nil {
			log.Printf("Error joining game: %s", err.Error())
			return
		}
	}
	// sleep for 30 seconds before "clicking" the start button
	time.Sleep(30 * time.Second)

	err = c.StartGameCommand(s, m, i, guild, activationSigil)
	if err != nil {
		log.Printf("Error starting game: %s", err.Error())
	}

	// use a go routine to play through the game
	go func() {
		for {
			log.Printf("Round %d", c.GameState.CurrentRound)
			time.Sleep(5 * time.Second)
			_, err := c.SendResponse(s, m, i, []string{"Playing round"}, false)
			if err != nil {
				return
			}
			// Find out who is the czar
			czar := c.GameState.CurrentCzar

			// Cycle through the players and tell them to select a card.
			for _, p := range c.GameState.Players {
				if p.DiscordID == czar.DiscordID {
					continue
				}
				c.HandleUserSubmission(1, DiscordInput{
					ChannelID: channelId,
					UserID:    p.DiscordID,
				}, s)
			}
			time.Sleep(5 * time.Second)
			_, err = c.SendResponse(s, m, i, []string{"Selecting winner"}, false)
			if err != nil {
				return
			}
			// Select a winner
			c.HandleCzarSubmission(1, DiscordInput{
				ChannelID: channelId,
				UserID:    czar.DiscordID,
			}, s)
			time.Sleep(5 * time.Second)
			if c.GameState.CurrentState == GameOver {
				_, err = c.SendResponse(s, m, i, []string{"Game over"}, false)
				if err != nil {
					return
				}
				return
			}
			_, err = c.SendResponse(s, m, i, []string{"Starting next round"}, false)
			if err != nil {
				return
			}
		}
	}()
}
