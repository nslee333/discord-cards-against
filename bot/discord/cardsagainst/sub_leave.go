package cardsagainst

import "fmt"

func (c *CardsAgainst) LeaveGameCommand(discordID string) (string, error) {
	c.mux.Lock()
	defer c.mux.Unlock()

	if !c.IsInPlayingState() {
		return "There is no game in progress!", nil
	}

	player := Player{DiscordID: discordID}
	if !c.PlayerInGame(player) {
		return fmt.Sprintf("<@%s> you are not in the game!", discordID), nil
	}
	c.RemovePlayer(player)
	return fmt.Sprintf("<@%s> you have left the game!", discordID), nil
}
