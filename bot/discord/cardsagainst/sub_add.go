package cardsagainst

import (
	"fmt"
	"log"
	"regexp"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func (c *CardsAgainst) AddPlayerCommand(
	s *discordgo.Session,
	m *discordgo.MessageCreate,
	i *discordgo.InteractionCreate,
	discordID string) (bool, error) {
	c.mux.Lock()
	defer c.mux.Unlock()

	if !c.IsInPlayingState() {
		success, err :=
			c.SendErrorMessage(s, m, i, []string{"You cannot add players, a game is not playing."})
		if err != nil {
			log.Printf("Add: Error sending message: %s", err.Error())
		}
		return success, err
	}

	if c.IsGameHost(Player{DiscordID: discordID}) {
		success, err := c.SendErrorMessage(s, m, i, []string{"Player is the game host."})
		if err != nil {
			log.Printf("Add: Error sending message: %s", err.Error())
		}
		return success, err
	}

	if c.PlayerInGame(Player{DiscordID: discordID}) {
		success, err := c.SendErrorMessage(s, m, i, []string{"Player is already in the game."})
		if err != nil {
			log.Printf("Add: Error sending message: %s", err.Error())
		}
		return success, err
	}

	userID, err := c.getUserIDFromDiscordID(discordID)

	if err != nil {
		return false, err
	}

	discordUser, err := s.User(userID)
	if err != nil {
		return false, err
	}

	c.addPlayerToLobby(discordUser)
	c.dealWhiteCards(discordUser.ID)

	c.updateJoinedPlayersMessage(s)
	c.sendAddPlayerMessage(s, discordUser.ID)

	return true, nil
}
func (c *CardsAgainst) SendErrorMessage(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, message []string) (bool, error) {
	_, err := c.SendResponse(s, m, i, message, true)
	if err != nil {
		log.Printf("Add: Error sending message: %s", err.Error())
	}
	return false, err
}

func (c *CardsAgainst) getUserIDFromDiscordID(discordID string) (string, error) {
	reg := regexp.MustCompile(`<@(?P<userid>[0-9]+)>`)
	match := reg.FindStringSubmatch(discordID)
	if len(match) == 0 {
		return "", fmt.Errorf("invalid user specified")
	}
	return match[1], nil
}

func (c *CardsAgainst) addPlayerToLobby(discordUser *discordgo.User) {
	c.GameState.PlayersInLobby = append(c.GameState.PlayersInLobby, Player{
		Name:      discordUser.Username,
		DiscordID: discordUser.ID,
		Score:     0,
		CardsUsed: []WhiteCard{},
	})
}

func (c *CardsAgainst) dealWhiteCards(discordID string) {
	for i := 0; i < WhiteCardCount; i++ {
		whiteCard := c.DealWhiteCard()
		c.GameState.PlayersInLobby[len(c.GameState.PlayersInLobby)-1].Hand = append(c.GameState.PlayersInLobby[len(c.GameState.PlayersInLobby)-1].Hand, whiteCard)
	}
}

func (c *CardsAgainst) updateJoinedPlayersMessage(s *discordgo.Session) {
	_, err := s.ChannelMessageEdit(c.GameState.Host.ChannelID, c.DiscordMessageIds["joined_players"], "Joined Users: "+strings.Join(c.MentionPlayerNames(), ", "))
	if err != nil {
		log.Printf("Add: Error sending message: %s", err.Error())
	}
}

func (c *CardsAgainst) sendAddPlayerMessage(s *discordgo.Session, discordID string) {
	_, err := s.ChannelMessageSend(c.GameState.Host.ChannelID, fmt.Sprintf("<@%s> will be added to the game next round!", discordID))
	if err != nil {
		log.Printf("Add: Error sending message: %s", err.Error())
	}
}
