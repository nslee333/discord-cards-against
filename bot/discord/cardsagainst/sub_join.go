package cardsagainst

import (
	"fmt"
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func (c *CardsAgainst) JoinGameCommand(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate) (string, error) {
	if c.GameState.CurrentState != WaitingForPlayers {
		return "No game is in progress!", fmt.Errorf("no game is in progress")
	}
	channelId := c.GetChannelId(m, i)
	// Join a game of Cards Against Humanity
	if m != nil {
		_ = c.AddNewPlayer(Player{
			Name:      m.Author.Username,
			DiscordID: m.Author.ID,
			ChannelID: channelId,
		})
	}
	if i != nil {
		_ = c.AddNewPlayer(Player{
			Name:      i.Member.User.Username,
			DiscordID: i.Member.User.ID,
			ChannelID: i.ChannelID,
		})
	}
	_, err := s.ChannelMessageEdit(
		channelId,
		c.DiscordMessageIds["joined_players"],
		"Joined Users: "+strings.Join(c.MentionPlayerNames(), ", "),
	)
	if err != nil {
		log.Printf("Join: Error editing message: %s", err.Error())
		return "error editing message", err
	}

	_, err = c.SendResponse(s, m, i, []string{"You have joined the game!"}, true)

	if err != nil {
		log.Printf("Join: Error sending message: %s", err.Error())
	}

	return "success", nil
}
