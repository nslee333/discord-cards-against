package cardsagainst

func (c *CardsAgainst) IsInPlayingState() bool {
	return c.GameState.CurrentState > 1 && c.GameState.CurrentState < 9
}

func (c *CardsAgainst) IsCardCzar(player Player) bool {
	return c.GameState.CurrentCzar.DiscordID == player.DiscordID
}

func (c *CardsAgainst) IsGameHost(player Player) bool {
	return c.GameState.Host.DiscordID == player.DiscordID
}
