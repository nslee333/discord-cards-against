package cardsagainst

import "fmt"

func (c *CardsAgainst) GameStatusCommand() string {

	// Get the number of players in the game
	numPlayers := len(c.GameState.Players)

	// Get the number of players in the lobby

	numPlayersInLobby := len(c.GameState.PlayersInLobby)

	// Get the current game state

	var gameState string

	switch c.GameState.CurrentState {
	case None:
		gameState = "None"
	case WaitingForPlayers:
		gameState = "Waiting for players"
	case GameStarting:
		gameState = "Game starting"
	case RoundStarting:
		gameState = "Round starting"
	case SelectingCzar:
		gameState = "Selecting czar"
	case WaitingForSubmissions:
		gameState = "Waiting for submissions"
	case ShowCzarSelections:
		gameState = "Show czar selections"
	case WaitingForCzarToPick:
		gameState = "Waiting for czar to pick"
	case DeclareGameWinner:
		gameState = "Declare game winner"
	case GameOver:
		gameState = "Game over"
	case Cancelled:
		gameState = "Game cancelled"
	}

	// Get the current round number

	roundNumber := c.GameState.CurrentRound

	// Get the current czar

	czar := c.GameState.CurrentCzar.DiscordID

	// Create a markdown string with the game status

	return fmt.Sprintf("```Game Status:\n\nPlayers: %d\nPlayers in lobby: %d\nGame state: %s\nRound number: %d\nCzar: <@%s>```", numPlayers, numPlayersInLobby, gameState, roundNumber, czar)
}
