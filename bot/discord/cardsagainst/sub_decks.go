package cardsagainst

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func (c *CardsAgainst) ConfigureDecksCommand(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	channelId := c.GetChannelId(m, i)

	// Configure the game of Cards Against Humanity
	// We need to get the number of points to win
	// The number of points to win is the first argument
	// If no arguments are passed, we default to 5 points to win

	// Create a two column markdown table
	if c.GameState.CurrentState != WaitingForPlayers {
		_, err := c.SendErrorMessage(s, m, i, []string{"You cannot configure decks, a game is already in progress."})
		if err != nil {
			log.Printf("Decks: Error sending message: %s", err.Error())
		}
		return err
	}
	deckstring := ""
	for index, deck := range c.Cards {
		if stringInSlice(index, c.GameState.SelectedPacks) {
			deckstring += fmt.Sprintf("✅ %d: %s\n", index+1, deck.Name)
		} else {
			deckstring += fmt.Sprintf("❌ %d: %s\n", index+1, deck.Name)
		}
		if len(deckstring) > 1900 {
			_, err := c.SendResponse(s, m, i, []string{deckstring}, true)

			if err != nil {
				log.Printf("Decks: Error sending message: %s", err.Error())
			}
			deckstring = ""
		}
	}

	// Send a message to the channel saying the game has started
	_, err := c.SendResponse(s, m, i, []string{deckstring}, true)

	if err != nil {
		log.Printf("Decks: Error sending message: %s", err.Error())
	}

	if len(arguments) == 0 {
		return err
	}

	// Arguments are the indexes of decks to toggle. Toggle them.
	for _, arg := range arguments {
		// Split CSV
		decks := strings.Split(arg, ",")
		for deckindex, deck := range decks {
			decknum, err := strconv.Atoi(deck)

			if err != nil {
				_, err = s.ChannelMessageSend(channelId, "Error parsing deck number: "+err.Error())
				if err != nil {
					log.Printf("Decks: Error sending message: %s", err.Error())
				}
			}

			if m != nil {

				responseMessageEmbed := &discordgo.MessageEmbed{
					Title:       "Decks Toggled",
					Description: "The following decks have been toggled:",
					Fields:      []*discordgo.MessageEmbedField{},
				}

				if stringInSlice(decknum, c.GameState.SelectedPacks) {
					c.GameState.SelectedPacks = remove(c.GameState.SelectedPacks, decknum)
					responseMessageEmbed.Fields = append(responseMessageEmbed.Fields, &discordgo.MessageEmbedField{
						Name:  fmt.Sprintf("Deck %d", decknum),
						Value: fmt.Sprintf("Deck %d has been disabled.", decknum),
					})
				} else {
					c.GameState.SelectedPacks = append(c.GameState.SelectedPacks, decknum)
					responseMessageEmbed.Fields = append(responseMessageEmbed.Fields, &discordgo.MessageEmbedField{
						Name:  fmt.Sprintf("Deck %d", decknum),
						Value: fmt.Sprintf("Deck %d has been enabled.", decknum),
					})
				}

				// If this is the last deck, send the message
				if deckindex == len(decks)-1 {
					// Respond to the message
					_, err = s.ChannelMessageSendEmbed(m.ChannelID, responseMessageEmbed)
					if err != nil {
						log.Printf("Decks: Error sending message: %s", err.Error())
					}
				}
			} else if i != nil {
				interactionResponse := discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Embeds: []*discordgo.MessageEmbed{
							{
								Title:       "Decks Toggled",
								Description: "The following decks have been toggled:",
								Fields:      []*discordgo.MessageEmbedField{},
							},
						},
					},
				}

				if stringInSlice(decknum, c.GameState.SelectedPacks) {
					c.GameState.SelectedPacks = remove(c.GameState.SelectedPacks, decknum)
					interactionResponse.Data.Embeds[0].Fields = append(interactionResponse.Data.Embeds[0].Fields, &discordgo.MessageEmbedField{
						Name:  fmt.Sprintf("Deck %d", decknum),
						Value: fmt.Sprintf("Deck %d has been disabled.", decknum),
					})

				} else {
					c.GameState.SelectedPacks = append(c.GameState.SelectedPacks, decknum)
					interactionResponse.Data.Embeds[0].Fields = append(interactionResponse.Data.Embeds[0].Fields, &discordgo.MessageEmbedField{
						Name:  fmt.Sprintf("Deck %d", decknum),
						Value: fmt.Sprintf("Deck %d has been enabled.", decknum),
					})
				}

				// If this is the last deck, send the message
				if deckindex == len(decks)-1 {
					err = s.InteractionRespond(i.Interaction, &interactionResponse)
					if err != nil {
						log.Printf("Decks: Error sending message: %s", err.Error())
					}
				}

				// Respond to the interaction
			}
		}
	}

	return err

}

func remove(s []int, i int) []int {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func stringInSlice(a int, list []int) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
