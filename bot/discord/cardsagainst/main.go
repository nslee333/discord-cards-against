package cardsagainst

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
	"go.mongodb.org/mongo-driver/mongo"
)

type CardsAgainst struct {
	Cards             Cards `json:"cards"`
	GameState         GameState
	DiscordMessageIds map[string]string
	mux               sync.Mutex
	Mongo             *mongo.Database
	Guild             *discordgo.Guild
}

const (
	// Game states
	None              = iota // No game is in progress
	WaitingForPlayers        // Waiting for players to join
	GameStarting             // Game is starting
	RoundStarting
	SelectingCzar
	WaitingForSubmissions // Waiting for players to submit their cards
	ShowCzarSelections
	WaitingForCzarToPick // Waiting for the czar to pick a winner
	DeclareGameWinner    // Declare the winner of the game
	GameOver             // Game is over
	Cancelled            // Game was cancelled
)

type GameState struct {
	Host                Player
	Players             []Player
	PlayedCardsWhite    []WhiteCard
	PlayedCardsBlack    []BlackCard
	CurrentBlackCard    BlackCard
	CurrentCzar         Player
	CurrentRound        int
	PointsToWin         int
	ShuffledCardsWhite  []WhiteCard
	ShuffledCardsBlack  []BlackCard
	SubmittedCardsWhite []SubmittedCard
	SelectedPacks       []int
	CurrentState        int
	DiscordInput        []DiscordInput
	Playing             bool
	PlayersInLobby      []Player
	PlayersFinished     map[string]bool
	ReadyMessageID      string
	ReadyMessage        string
	NextTransition      time.Time
	UpdateWindowId      string
	BurnedCards         []WhiteCard
}

type SubmittedCard struct {
	Player      Player
	Card        []WhiteCard
	PlayerReady bool
}

type DiscordInput struct {
	ServerID  string
	ChannelID string
	Message   string
	UserID    string
	MessageID string
}

type Player struct {
	Name      string
	Hand      []WhiteCard
	DiscordID string
	Score     int
	CardsUsed []WhiteCard
	ChannelID string
}

type WhiteCard struct {
	Text string
	Pack int
}

type BlackCard struct {
	Text string
	Pick int
}

type Cards []struct {
	Name  string `json:"name"`
	White []struct {
		Text string `json:"text"`
		Pack int    `json:"pack"`
	} `json:"white"`
	Black []struct {
		Text string `json:"text"`
		Pick int    `json:"pick"`
		Pack int    `json:"pack"`
	} `json:"black"`
	Official bool `json:"official"`
}

func New(ctx *context.Context, guild *discordgo.Guild, mongo *mongo.Database) *CardsAgainst {

	cah := new(CardsAgainst)
	cah.Guild = guild
	cah.Cards = make(Cards, 0)
	// Load the cards from the base directory data/cah-all-full.json

	fileReader, err := os.Open("../data/cah-all-full.json")
	if err != nil {
		log.Printf("Error sending message: %s", err)
	}
	rawJSON, err := io.ReadAll(fileReader)
	if err != nil {
		log.Printf("Error sending message: %s", err)
	}
	err = json.Unmarshal(rawJSON, &cah.Cards)
	if err != nil {
		log.Printf("Error sending message: %s", err)
	}

	cah.Mongo = mongo

	cah.GameState = GameState{
		Host:                Player{},
		Players:             make([]Player, 0),
		PlayedCardsWhite:    make([]WhiteCard, 0),
		PlayedCardsBlack:    make([]BlackCard, 0),
		CurrentBlackCard:    BlackCard{},
		CurrentCzar:         Player{},
		CurrentRound:        0,
		PointsToWin:         5,
		ShuffledCardsWhite:  make([]WhiteCard, 0),
		ShuffledCardsBlack:  make([]BlackCard, 0),
		SubmittedCardsWhite: make([]SubmittedCard, 0),
		SelectedPacks:       make([]int, 0),
		CurrentState:        None,
		DiscordInput:        make([]DiscordInput, 0),
		Playing:             false,
		PlayersInLobby:      make([]Player, 0),
		PlayersFinished:     make(map[string]bool),
		ReadyMessageID:      "",
		ReadyMessage:        "",
		NextTransition:      time.Now(),
		UpdateWindowId:      "",
	}

	cah.DiscordMessageIds = make(map[string]string)

	for i := 0; i < len(cah.Cards); i++ {
		cah.GameState.SelectedPacks = append(cah.GameState.SelectedPacks, i)
	}

	return cah
}

func (c *CardsAgainst) UpdateInputFromDiscord(serverId string, channelId string, message string, userId string, messageID string) {
	// Get the channel from Discord
	// Get the last message in the channel
	// Parse the message
	c.GameState.DiscordInput = append(c.GameState.DiscordInput, DiscordInput{
		ServerID:  serverId,
		ChannelID: channelId,
		Message:   message,
		UserID:    userId,
		MessageID: messageID,
	})
}

func NewContext() *context.Context {
	ctx := context.Background()
	return &ctx
}

func (c *CardsAgainst) InteractionHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {

	// Handle the interaction
	// Check if the interaction is a button press
	// Check if the interaction is a select menu
	// Check if the interaction is a slash command
	// Check if the interaction is a context menu
	// Send the user a list of cards if the interaction is a button press on the id "show_cards"

	switch i.Interaction.MessageComponentData().CustomID {
	case "show_cards":
		// Respond to the interaction with a list of the user's cards
		// Get the user's cards
		var cards string

		if c == nil || c.GameState.CurrentState != WaitingForSubmissions {
			// The game is not in the waiting for submissions state, the user can't submit cards
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "Can't show your cards right now, we're not waiting for submissions!",
					Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
				},
			})
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}
			return
		}
		if c.GameState.CurrentCzar.DiscordID == i.Interaction.Member.User.ID {
			// Player is the czar, they can't submit cards tell them to wait
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "You are the czar, you can't submit cards!",
					Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
				},
			})
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}
			return
		}

		for _, player := range c.GameState.Players {
			if player.DiscordID == i.Interaction.Member.User.ID {
				for i, card := range player.Hand {
					cards = cards + fmt.Sprintf("%d. %s\n", i, card.Text)
				}
			}
		}
		// Send the user a list of their cards using ephermal messages
		err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: fmt.Sprintf("Black Card: \"%s\"\n", c.GameState.CurrentBlackCard.Text) + "Your cards are:\n" + cards,
				Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
			},
		})
		if err != nil {
			log.Printf("Error sending message: %s", err)
		}
		return
	case "leaderboard":
		err := c.GenerateScoreboard(s)
		if err == nil {
			data, err := os.ReadFile(fmt.Sprintf("scoreboard-%s.png", c.Guild.ID))
			var file *discordgo.File
			if err == nil {

				// Create a new File struct for the image
				file = &discordgo.File{
					Name:   "scoreboard.png",
					Reader: bytes.NewReader(data),
				}

			}

			err = s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,

				Data: &discordgo.InteractionResponseData{
					Content: "The current scoreboard:",
					Files: []*discordgo.File{
						file,
					},
				},
			})
			if err != nil {
				log.Printf("Error sending message: %v", err)
			}
		} else {
			err = s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "Error generating scoreboard, this might mean that no games have been played yet.",
					Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
				},
			})

			if err != nil {
				log.Printf("Error sending message: %v", err)
			}

		}
	case "selftest":
		if c == nil || c.IsInPlayingState() {
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Flags: discordgo.MessageFlagsEphemeral,
					Embeds: []*discordgo.MessageEmbed{
						{
							Title: "Self Test",
							Fields: []*discordgo.MessageEmbedField{
								{
									Name:  "Game State",
									Value: c.GameStatusToEnglish(),
								},
								{
									Name:  "Error",
									Value: "Can't run self test while game is in progress",
								},
							},
						},
					},
				},
			})
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}
			return
		}
	case "join_game":
		// Add the user to the game
		// Check if the game is in progress
		if c == nil || c.GameState.CurrentState != WaitingForPlayers {
			err := s.InteractionRespond(
				i.Interaction,
				&discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,

					Data: &discordgo.InteractionResponseData{
						Content: "You can't join the game right now! Is the game started? Are you even playing?",
						Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
					},
				},
			)
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}

		}

		_ = c.AddNewPlayer(Player{
			Name:      i.Interaction.Member.User.Username,
			DiscordID: i.Interaction.Member.User.ID,
			ChannelID: i.ChannelID,
		})

		_, err := s.ChannelMessageEdit(i.ChannelID, c.DiscordMessageIds["joined_players"], "Joined Users: "+strings.Join(c.MentionPlayerNames(), ", "))
		if err != nil {
			log.Printf("Error sending message: %s", err)
		}

		err = s.InteractionRespond(
			i.Interaction,
			&discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: fmt.Sprintf("%s has joined the game!", i.Interaction.Member.User.Username),
					Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
				},
			},
		)
		if err != nil {
			log.Printf("Error sending message: %s", err)
		}
		return

	case "cancel_game":
		// Cancel the game
		// Check if the user is the host
		if c == nil || c.GameState.CurrentState != WaitingForPlayers {
			err := s.InteractionRespond(
				i.Interaction,
				&discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,

					Data: &discordgo.InteractionResponseData{
						Content: "You can't cancel the game right now! Is the game started? Are you even playing?",
						Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
					},
				},
			)
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}
			return
		}
		if c.GameState.Host.DiscordID != i.Interaction.Member.User.ID {
			err := s.InteractionRespond(
				i.Interaction,
				&discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,

					Data: &discordgo.InteractionResponseData{
						Content: "Only the host can cancel the game!",
						Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
					},
				},
			)
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}
			return
		}
		// Cancel the game
		c.GameState.CurrentState = Cancelled

		err := s.InteractionRespond(
			i.Interaction,
			&discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,

				Data: &discordgo.InteractionResponseData{
					Content: "The game has been cancelled!",
					Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
				},
			},
		)
		if err != nil {
			log.Printf("Error sending message: %s", err)
		}
		return

	case "start_game":
		// Start the game
		// Check if the user is the host
		if c == nil || c.GameState.CurrentState != WaitingForPlayers {
			text := "You can't start the game right now! Because:"
			switch c.GameState.CurrentState {
			case None:
				text = text + " The game hasn't been started yet!"
			case GameStarting:
				text = text + " The game is already starting!"
			case RoundStarting:
				text = text + " The round is already starting!"
			case SelectingCzar:
				text = text + " The czar is already selecting a card!"
			case WaitingForSubmissions:
				text = text + " The game is already waiting for submissions!"
			case ShowCzarSelections:
				text = text + " The czar is already selecting a winner!"
			case WaitingForCzarToPick:
				text = text + " The czar is already selecting a winner!"
			case DeclareGameWinner:
				text = text + " The game is already over!"
			case GameOver:
				text = text + " The game is already over!"
			case Cancelled:
				text = text + " The game has been cancelled!"
			}
			err := s.InteractionRespond(
				i.Interaction,
				&discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,

					Data: &discordgo.InteractionResponseData{
						Content: text,
						Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
					},
				},
			)
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}
			return
		}
		if c.GameState.Host.DiscordID != i.Interaction.Member.User.ID {
			err := s.InteractionRespond(
				i.Interaction,
				&discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,

					Data: &discordgo.InteractionResponseData{
						Content: "Only the host can start the game!",
						Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
					},
				},
			)
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}
			return
		}
		// Requires a minimum of 3 players
		if len(c.GameState.Players) < 3 {
			err := s.InteractionRespond(
				i.Interaction,
				&discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,

					Data: &discordgo.InteractionResponseData{
						Content: "You need at least 3 players to start the game!",
						Flags:   discordgo.MessageFlagsEphemeral, // Make the message ephemeral
					},
				},
			)
			if err != nil {
				log.Printf("Error sending message: %s", err)
			}
			return
		}
		// Lock the game state players can only be added by host now

		err := s.InteractionRespond(
			i.Interaction,
			&discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "The game is now locked. Only the host can add players.\nStarting the game in 10 seconds...",
				},
			},
		)
		if err != nil {
			log.Printf("Error sending message: %s", err)
		}

		c.GameState.CurrentState = GameStarting
		// Sleep for 10 seconds, start the game loop
		time.Sleep(10 * time.Second)
		go c.GameLoop(s, i.Interaction.ChannelID)

	}

}

func (c *CardsAgainst) GameStatusToEnglish() string {
	switch c.GameState.CurrentState {
	case None:
		return "No game is in progress"
	case WaitingForPlayers:
		return "Waiting for players to join"
	case GameStarting:
		return "Game is starting"
	case RoundStarting:
		return "Round is starting"
	case SelectingCzar:
		return "Selecting czar"
	case WaitingForSubmissions:
		return "Waiting for submissions"
	case ShowCzarSelections:
		return "Showing czar selections"
	case WaitingForCzarToPick:
		return "Waiting for czar to pick"
	case DeclareGameWinner:
		return "Declaring game winner"
	case GameOver:
		return "Game is over"
	case Cancelled:
		return "Game was cancelled"
	}
	return ""
}

func (c *CardsAgainst) ResetGame() {

	c.Cards = make(Cards, 0)
	// Load the cards from the base directory data/cah-all-full.json

	fileReader, err := os.Open("../data/cah-all-full.json")
	if err != nil {
		log.Printf("Error sending message: %s", err)
	}
	rawJSON, err := io.ReadAll(fileReader)
	if err != nil {
		log.Printf("Error sending message: %s", err)
	}
	err = json.Unmarshal(rawJSON, &c.Cards)
	if err != nil {
		log.Printf("Error sending message: %s", err)
	}

	c.GameState = GameState{
		Host:                Player{},
		Players:             make([]Player, 0),
		PlayedCardsWhite:    make([]WhiteCard, 0),
		PlayedCardsBlack:    make([]BlackCard, 0),
		CurrentBlackCard:    BlackCard{},
		CurrentCzar:         Player{},
		CurrentRound:        0,
		PointsToWin:         5,
		ShuffledCardsWhite:  make([]WhiteCard, 0),
		ShuffledCardsBlack:  make([]BlackCard, 0),
		SubmittedCardsWhite: make([]SubmittedCard, 0),
		SelectedPacks:       make([]int, 0),
		CurrentState:        None,
		DiscordInput:        make([]DiscordInput, 0),
		Playing:             false,
		PlayersInLobby:      make([]Player, 0),
		PlayersFinished:     make(map[string]bool),
		ReadyMessageID:      "",
		ReadyMessage:        "",
		NextTransition:      time.Now(),
		UpdateWindowId:      "",
	}

	c.DiscordMessageIds = make(map[string]string)

	for i := 0; i < len(c.Cards); i++ {
		c.GameState.SelectedPacks = append(c.GameState.SelectedPacks, i)
	}
}

func (c *CardsAgainst) GetChannelId(m *discordgo.MessageCreate, i *discordgo.InteractionCreate) string {
	if m != nil {
		return m.ChannelID
	}
	if i != nil {
		return i.ChannelID
	}
	return ""
}

func (c *CardsAgainst) SendResponse(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string, ephermal bool) (*discordgo.Message, error) {
	if m != nil {
		// Respond to MessageCreate event
		msg, err := s.ChannelMessageSend(m.ChannelID, strings.Join(arguments, " "))
		if err != nil {
			log.Printf("Error sending message: %s", err.Error())
			return nil, err
		}
		return msg, nil
	} else if i != nil {
		if ephermal {
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: strings.Join(arguments, " "),
					Flags:   discordgo.MessageFlagsEphemeral, // Make the response ephemeral
				},
			})
			if err != nil {
				log.Printf("Error responding to interaction: %s", err.Error())
				return nil, err
			}
			return nil, nil
		} else {

			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: strings.Join(arguments, " "),
					Flags:   discordgo.MessageFlagsEphemeral, // Make the response ephemeral
				},
			})
			if err != nil {
				log.Printf("Error responding to interaction: %s", err.Error())
				return nil, err
			}
			return nil, nil
		}
	}
	return nil, nil

}

func (c *CardsAgainst) SendComplexMessage(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, message *discordgo.MessageSend, ephermal bool) (*discordgo.Message, error) {
	if m != nil {
		// Respond to MessageCreate event
		msg, err := s.ChannelMessageSendComplex(m.ChannelID, message)
		if err != nil {
			log.Printf("Error sending message: %s", err.Error())
			return nil, err
		}
		return msg, nil
	} else if i != nil {
		log.Printf("Sending complex message: %+v", message.Embeds)
		// Convert the *discordgo.MessageSend to something compatible with an interaction response

		if ephermal {
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content:         message.Content,
					Embeds:          message.Embeds,
					Components:      message.Components,
					AllowedMentions: message.AllowedMentions,
					Files:           message.Files,
					TTS:             message.TTS,

					Flags: discordgo.MessageFlagsEphemeral, // Make the response ephemeral
				},
			})
			if err != nil {
				log.Printf("Error responding to interaction: %s", err.Error())
				return nil, err
			}
			return nil, nil
		} else {

			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content:         message.Content,
					Embeds:          message.Embeds,
					Components:      message.Components,
					Files:           message.Files,
					TTS:             message.TTS,
					AllowedMentions: message.AllowedMentions,
				},
			})
			if err != nil {
				log.Printf("Error responding to interaction: %s", err.Error())
				return nil, err
			}
			return nil, nil
		}
	}
	return nil, nil

}

func (c *CardsAgainst) GetAuthorId(m *discordgo.MessageCreate, i *discordgo.InteractionCreate) string {
	if m != nil {
		return m.Author.ID
	}
	if i != nil {
		return i.Member.User.ID
	}
	return ""
}
