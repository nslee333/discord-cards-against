package discord

import (
	"context"
	"fmt"
	"log"

	"github.com/bwmarrin/discordgo"
	"go.mongodb.org/mongo-driver/bson"
)

func (b *Bot) HandleServerPreferences(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	// Gets  server preferences:
	// announce channel
	// turn off startup announcement
	// set the activation sigil (default ^)

	// Get the guild
	var guild *discordgo.Guild
	var err error

	if m != nil {
		guild, err = s.Guild(m.GuildID)
	} else if i != nil {
		guild, err = s.Guild(i.GuildID)
	}

	if err != nil {
		log.Printf("Error getting guild: %s", err.Error())
		return err
	}

	log.Printf("Guild: %s", guild.Name)

	authorId := ""

	if m != nil {
		authorId = m.Author.ID
	} else if i != nil {
		authorId = i.Member.User.ID
	}

	// The user must be an admin to get server preferences
	isAdmin, err := b.IsUserAdmin(s, guild, authorId)

	if err != nil {
		log.Printf("Error checking if user is admin: %s", err.Error())
		return err
	}

	if !isAdmin {
		_, err := b.RespondToEvent(s, m, i, "You must be an admin to get server preferences", true)
		if err != nil {
			log.Printf("GetServerPreferences: Error sending message: %s", err.Error())
		}
		return err
	}

	// Get the server preferences from mongodb

	prefsDatabase := b.MongoClient.Database(ConvertGuildNameToDBName(guild.Name))
	prefsCollection := prefsDatabase.Collection("preferences")

	var prefs bson.M

	err = prefsCollection.FindOne(context.Background(), bson.M{}).Decode(&prefs)

	if err != nil {
		// Set the default preferences
		prefs = bson.M{
			"announce_channel":     "#bot-testing",
			"startup_announcement": true,
			"activation_sigil":     "^",
		}
	}

	// The bare preferences command just sends the preferences to the user

	if len(arguments) == 0 {
		// This is a "get" the preferences, use mongodb to get the preferences
		_, err := b.RespondToEvent(s, m, i, fmt.Sprintf("Server preferences:\nAnnounce Channel: %s\nStartup Announcement: %t\nActivation Sigil: %s", prefs["announce_channel"], prefs["startup_announcement"], prefs["activation_sigil"]), true)
		if err != nil {
			log.Printf("GetServerPreferences: Error sending message: %s", err.Error())
		}
		return err
	}

	subcommand := arguments[0]
	arguments = arguments[1:]

	switch subcommand {
	case "announce":
		// Set the announce channel
		if len(arguments) == 0 {
			_, err := b.RespondToEvent(s, m, i, fmt.Sprintf("Announce channel is currently: %s", prefs["announce_channel"]), true)
			if err != nil {
				log.Printf("GetServerPreferences: Error sending message: %s", err.Error())
			}
			return err
		}
		prefs["announce_channel"] = arguments[0]
		_, err = b.RespondToEvent(s, m, i, fmt.Sprintf("Announce channel set to %s", arguments[0]), true)
		if err != nil {
			log.Printf("SetServerPreferences: Error sending message: %s", err.Error())
		}
	case "startup":
		if len(arguments) == 0 {
			_, err := b.RespondToEvent(s, m, i, fmt.Sprintf("Startup announcement is currently: %v", prefs["startup_announcement"]), true)
			if err != nil {
				log.Printf("GetServerPreferences: Error sending message: %s", err.Error())
			}
			return err
		}
		// Turn on or off the startup announcement
		prefs["startup_announcement"] = arguments[0]
		_, err = b.RespondToEvent(s, m, i, fmt.Sprintf("Startup announcement set to %v", arguments[0]), true)
		if err != nil {
			log.Printf("SetServerPreferences: Error sending message: %s", err.Error())
		}
	case "prefix":
		if len(arguments) == 0 {
			_, err := b.RespondToEvent(s, m, i, fmt.Sprintf("Activation sigil is currently: %s", prefs["activation_sigil"]), true)
			if err != nil {
				log.Printf("GetServerPreferences: Error sending message: %s", err.Error())
			}
			return err
		}
		// Set the activation sigil
		prefs["activation_sigil"] = arguments[0]
		_, err = b.RespondToEvent(s, m, i, fmt.Sprintf("Activation sigil set to %s", arguments[0]), true)
		if err != nil {
			log.Printf("SetServerPreferences: Error sending message: %s", err.Error())
		}
	default:
		_, err := b.RespondToEvent(s, m, i, "Invalid subcommand. Valid subcommands are: `announce`, `startup`, `prefix`", true)
		if err != nil {
			log.Printf("GetServerPreferences: Error sending message: %s", err.Error())
		}
		return err
	}

	_, err = prefsCollection.ReplaceOne(context.Background(), bson.M{}, prefs)

	if err != nil {
		log.Printf("Error updating server preferences: %s", err.Error())
		return err
	}

	return nil
}

func (b *Bot) IsUserAdmin(s *discordgo.Session, guild *discordgo.Guild, userId string) (bool, error) {
	// Check if the user is an admin
	if userId == guild.OwnerID {
		return true, nil
	}
	// Get the member using the session and the guild and user IDs.
	member, err := s.GuildMember(guild.ID, userId)
	if err != nil {
		// Handle the error according to your use case.
		return false, err
	}

	// Iterate through the roles in the guild.
	for _, role := range guild.Roles {
		// Check if the role has the PermissionAdministrator flag set.
		if role.Permissions&discordgo.PermissionAdministrator != 0 {
			// Iterate through the user's roles to see if they have the admin role.
			for _, memberRoleID := range member.Roles {
				if memberRoleID == role.ID {
					log.Printf("User %s is an admin", member.User.Username)
					return true, nil
				}
			}
		}
	}

	// If none of the user's roles had the admin permission, the user is not an admin.
	return false, nil
}

func (b *Bot) GetServerPreferences(s *discordgo.Session, guildName string) (bson.M, error) {
	// Get the server preferences from mongodb

	prefsDatabase := b.MongoClient.Database(ConvertGuildNameToDBName(guildName))
	prefsCollection := prefsDatabase.Collection("preferences")

	var prefs bson.M

	err := prefsCollection.FindOne(context.Background(), bson.M{}).Decode(&prefs)

	if err != nil {
		// Set the default preferences
		prefs = bson.M{
			"announce_channel":     "#bot-testing",
			"startup_announcement": true,
			"activation_sigil":     "^",
		}
	}

	return prefs, nil
}
