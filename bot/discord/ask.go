package discord

import (
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/mchineboy/cahbot/bot/discord/ask"
)

func (b *Bot) Ask(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	// Show the message, consulting the oracle and https://giphy.com/stickers/goodfortunesonly-good-fortune-fortunes-only-UyRnjWSkikBTU2SMOm
	channelId := b.GetChannelId(m, i)

	var message *discordgo.Message
	var err error
	// If this was an InteractionCreate event, we need to respond to it
	if i != nil {
		// Respond to InteractionCreate event
		response := discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "Consulting the Oracle",
				Embeds: []*discordgo.MessageEmbed{
					{
						Title: "Consulting the Oracle",
						Fields: []*discordgo.MessageEmbedField{
							{
								Name: "Please Wait...",
							},
						},
						Thumbnail: &discordgo.MessageEmbedThumbnail{
							URL: "https://media.giphy.com/media/UyRnjWSkikBTU2SMOm/giphy.gif",
						},
					},
				},
			},
		}

		err := s.InteractionRespond(i.Interaction, &response)

		if err != nil {
			log.Printf("Error responding to interaction: %s", err.Error())
			return err
		}
	} else if m != nil {
		embed := &discordgo.MessageEmbed{
			Title: "Consulting the Oracle",
			Fields: []*discordgo.MessageEmbedField{
				{
					Name: "Please Wait...",
				},
			},
			Thumbnail: &discordgo.MessageEmbedThumbnail{
				URL: "https://media.giphy.com/media/UyRnjWSkikBTU2SMOm/giphy.gif",
			},
		}

		messageSend := discordgo.MessageSend{
			Embed: embed,
		}

		// Send announcement message
		message, err = s.ChannelMessageSendComplex(
			channelId,
			&messageSend,
		)

		if err != nil {
			log.Printf("ask: Error sending message: %s", err.Error())
			return err
		}
	}
	go func() {
		username := ""

		if m != nil {
			username = m.Author.Username
		} else if i != nil {
			username = i.Member.User.Username
		}

		discordId := ""
		if m != nil {
			discordId = m.Author.ID
		} else if i != nil {
			discordId = i.Member.User.ID
		}

		err := s.ChannelTyping(channelId)

		if err != nil {
			log.Printf("ask: Error sending typing indicator: %s", err.Error())
		}

		horoString := ask.AskGPT4(username, strings.Join(arguments, " "))

		// Replace the user's name with an <@id> mention
		horoString = strings.ReplaceAll(horoString, username, "<@"+discordId+">")
		if message != nil {
			err = s.ChannelMessageDelete(channelId, message.ID)

			if err != nil {
				log.Printf("Error deleting message: %s", err.Error())
				return
			}
		}

		if horoString != "" {
			// Max length to send to discord is 2000 characters, split the string into multiple messages
			words := strings.Split(horoString, " ")

			horoReturn := ""

			for _, word := range words {
				horoReturn += word + " "
				if len(horoReturn) > 1750 {
					_, err := s.ChannelMessageSend(channelId, horoReturn)
					if err != nil {
						log.Printf("ask Partial: Error sending message: %s", err.Error())
					}
					horoReturn = ""
				}
			}

			_, err := s.ChannelMessageSend(channelId, horoReturn)

			if err != nil {
				log.Printf("ask: Error sending message: %s", err.Error())
			}

		}
	}()

	return nil
}
