package discord

import (
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/mchineboy/cahbot/bot/discord/cardsagainst"
)

// This code is a mess and needs some serious refactoring

func (b *Bot) HumansAgainstPlasticRectangles(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, arguments []string) error {
	// Get the guild
	var guild *discordgo.Guild
	var err error

	if m != nil {
		guild, err = s.Guild(m.GuildID)
	} else if i != nil {
		guild, err = s.Guild(i.GuildID)
	}

	if err != nil {
		log.Printf("Error getting guild: %s", err.Error())
		return err
	}

	channelId := ""

	if m != nil {
		channelId = m.ChannelID
	} else if i != nil {
		channelId = i.ChannelID
	}

	log.Printf("Guild: %s", guild.Name)

	if games[channelId] == nil {
		ctx := cardsagainst.NewContext()
		games[channelId] = cardsagainst.New(ctx, guild, b.MongoClient.Database(ConvertGuildNameToDBName(guild.Name)))
	}
	// If the command is "cah", we need to figure out what subcommand was passed
	if len(arguments) == 0 {
		_, err := b.RespondToEvent(s, m, i, "You must specify a subcommand. Valid subcommands are: `start`, `join`, `leave`, `end`, `status`, `reset`, `kick`, `points`, `decks`", true)
		//channelId, "You must specify a subcommand. Valid subcommands are: `start`, `join`, `leave`, `end`, `status`")
		if err != nil {
			log.Printf("CAH Missing Argument: Error sending message: %s", err.Error())
		}
		return err
	}

	subcommand := arguments[0]
	arguments = arguments[1:]

	switch subcommand {
	case "kick":
		_, err = games[channelId].KickPlayerCommand(s, m, i, arguments[0])
		if err != nil {
			log.Printf("Kick: Error kicking player: %s", err.Error())
			return err
		}
		return nil
	case "end":
		_, err = games[channelId].EndGameCommand(s, m, i)
		if err != nil {
			log.Printf("End: Error ending game: %s", err.Error())
			return err
		}
		return nil
	case "add":
		_, err = games[channelId].AddPlayerCommand(s, m, i, arguments[0])
		if err != nil {
			log.Printf("Add: Error adding player: %s", err.Error())
			return err
		}
		return nil
	case "status":
		statusString := games[channelId].GameStatusCommand()
		if err != nil {
			log.Printf("Status: Error getting game status: %s", err.Error())
			return err
		}
		_, err = b.RespondToEvent(s, m, i, statusString, true)
		if err != nil {
			log.Printf("Status: Error sending message: %s", err.Error())
			return err
		}
		return nil
	case "join":
		_, err = games[channelId].JoinGameCommand(s, m, i)
		if err != nil {
			log.Printf("Join: Error joining game: %s", err.Error())
			return err
		}
		return nil
	case "leave":
		message, err := games[channelId].LeaveGameCommand(m.Author.ID)
		if err != nil {
			log.Printf("Leave: Error leaving game: %s", err.Error())
			return err
		}
		if message != "" {
			_, err = s.ChannelMessageSend(channelId, message)
			if err != nil {
				log.Printf("Leave: Error sending message: %s", err.Error())
				return err
			}
		}
		return nil
	case "points":
		err = games[channelId].SetGamePoints(s, m, i, arguments)
		if err != nil {
			log.Printf("Points: Error setting game points: %s", err.Error())
			return err
		}
		return nil
	case "decks":
		err = games[channelId].ConfigureDecksCommand(s, m, i, arguments)
		if err != nil {
			log.Printf("Decks: Error configuring decks: %s", err.Error())
			return err
		}
		return nil
	case "reset":
		games[channelId].ResetCommand(s, m, i)
		return nil
	case "start":
		err = games[channelId].StartGameCommand(s, m, i, guild, b.ActivationSigil)
		if err != nil {
			log.Printf("Start: Error starting game: %s", err.Error())
			return err
		}
	case "selftest":
		games[channelId].TestGame(s, m, i, guild, b.ActivationSigil)
		return nil
	case "leaderboard":
		_, err = games[channelId].LeaderboardCommand(s, m, i)
		if err != nil {
			log.Printf("Leaderboard: Error getting leaderboard: %s", err.Error())
			return err
		}
		return nil
	case "trade":
		success, err := games[channelId].TradePointsForNewCards(s, m, i)
		if err != nil {
			log.Printf("Trade: Error trading points: %s", err.Error())
			return err
		}
		if success {
			_, err = b.RespondToEvent(s, m, i, "You have traded two points for new cards.", true)
			if err != nil {
				log.Printf("Trade: Error sending message: %s", err.Error())
				return err
			}
		}
	}

	return nil
}

func ConvertGuildNameToDBName(guildName string) string {
	// Replace invalid characters with an underscore
	dbName := strings.ReplaceAll(guildName, "/", "_")
	dbName = strings.ReplaceAll(dbName, "\\", "_")
	dbName = strings.ReplaceAll(dbName, ".", "_")
	dbName = strings.ReplaceAll(dbName, "\"", "_")
	dbName = strings.ReplaceAll(dbName, "*", "_")
	dbName = strings.ReplaceAll(dbName, "<", "_")
	dbName = strings.ReplaceAll(dbName, ">", "_")
	dbName = strings.ReplaceAll(dbName, ":", "_")
	dbName = strings.ReplaceAll(dbName, "|", "_")
	dbName = strings.ReplaceAll(dbName, "?", "_")
	dbName = strings.ReplaceAll(dbName, "$", "_")
	dbName = strings.ReplaceAll(dbName, " ", "_")
	dbName = strings.ReplaceAll(dbName, "\n", "_")
	dbName = strings.ReplaceAll(dbName, "\r", "_")
	dbName = strings.ReplaceAll(dbName, "\t", "_")
	dbName = strings.ReplaceAll(dbName, "\x00", "_")
	dbName = strings.ReplaceAll(dbName, "\f", "_")
	dbName = strings.ReplaceAll(dbName, "\v", "_")
	dbName = strings.ReplaceAll(dbName, "\b", "_")

	// Ensure the name does not start with a dollar sign
	if strings.HasPrefix(dbName, "$") {
		dbName = "_" + dbName
	}

	dbName = strings.ToLower(dbName)
	// Ensure the name is not too long should account for emoji too
	if len(dbName) > 60 {
		dbName = dbName[:60]
	}

	return dbName
}
