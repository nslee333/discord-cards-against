package discord

import (
	"log"

	"github.com/bwmarrin/discordgo"
)

func (b *Bot) RespondToEvent(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, content string, ephermal bool) (*discordgo.Message, error) {
	if m != nil {
		// Respond to MessageCreate event
		msg, err := s.ChannelMessageSend(m.ChannelID, content)
		if err != nil {
			log.Printf("Error sending message: %s", err.Error())
			return nil, err
		}
		return msg, nil
	} else if i != nil {
		if ephermal {
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: content,
					Flags:   discordgo.MessageFlagsEphemeral, // Make the response ephemeral
				},
			})
			if err != nil {
				log.Printf("Error responding to interaction: %s", err.Error())
				return nil, err
			}
			return nil, nil
		} else {

			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: content,
					Flags:   discordgo.MessageFlagsEphemeral, // Make the response ephemeral
				},
			})
			if err != nil {
				log.Printf("Error responding to interaction: %s", err.Error())
				return nil, err
			}
			return nil, nil
		}
	}
	return nil, nil
}

func (b *Bot) ResponseToEventComplex(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate, message *discordgo.MessageSend, ephermal bool) (*discordgo.Message, error) {
	if m != nil {
		// Respond to MessageCreate event
		msg, err := s.ChannelMessageSendComplex(m.ChannelID, message)
		if err != nil {
			log.Printf("Error sending message: %s", err.Error())
			return nil, err
		}
		return msg, nil
	} else if i != nil {
		if ephermal {
			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: message.Content,
					Flags:   discordgo.MessageFlagsEphemeral, // Make the response ephemeral
				},
			})
			if err != nil {
				log.Printf("Error responding to interaction: %s", err.Error())
				return nil, err
			}
			return nil, nil
		} else {

			err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: message.Content,
					Flags:   discordgo.MessageFlagsEphemeral, // Make the response ephemeral
				},
			})
			if err != nil {
				log.Printf("Error responding to interaction: %s", err.Error())
				return nil, err
			}
			return nil, nil
		}
	}
	return nil, nil
}

func (b *Bot) GetUser(s *discordgo.Session, m *discordgo.MessageCreate, i *discordgo.InteractionCreate) (*discordgo.User, error) {
	if m != nil {
		// Respond to MessageCreate event
		user, err := s.User(m.Author.ID)
		if err != nil {
			log.Printf("Error getting user: %s", err.Error())
			return nil, err
		}
		return user, nil
	} else if i != nil {
		user, err := s.User(i.Member.User.ID)
		if err != nil {
			log.Printf("Error getting user: %s", err.Error())
			return nil, err
		}
		return user, nil
	}
	return nil, nil
}
