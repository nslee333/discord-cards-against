# Development Stage
FROM golang:alpine AS dev
WORKDIR /app
COPY . .

RUN apk add -U nodejs npm
RUN npm install -g nodemon
WORKDIR /app/bot
# nodemon --watch './**/*.go' --signal SIGTERM --exec 'go' run cmd/MyProgram/main.go
CMD ["nodemon", "--delay", "10", "-L", "--watch", "./**/*.go", "--signal", "SIGTERM", "--exec", "go", "run", "main.go"]

# Production Stage
FROM golang:alpine as build
WORKDIR /app
COPY . .
RUN go build -o dist/main bot/main.go
WORKDIR /app/dist
CMD ["./main"]

FROM golang:alpine as prod
COPY --from=build /app/dist/main /app/dist/main
COPY --from=build /app/data /app/data
COPY --from=build /app/fonts /app/fonts
COPY --from=build /app/releasenotes.json /app/releasenotes.json
COPY --from=build /app/hosts /etc/hosts
COPY --from=build /app/resolv.conf /etc/resolv.conf
WORKDIR /app/dist
EXPOSE 6060
CMD ["./main"]
